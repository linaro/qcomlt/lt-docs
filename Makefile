# required so that further += assignment are immediate, not defered...
TEXTILE:=
include debian/boards/snapdragon/Makefile
include debian/boards/dragonboard410c/Makefile
include debian/boards/dragonboard845c/Makefile
include debian/boards/rb5/Makefile
include openembedded/boards/dragonboard410c/Makefile
include openembedded/boards/dragonboard845c/Makefile
include openembedded/boards/rb5/Makefile
include openembedded/boards/sa8155p-adp/Makefile

HTML=$(subst .textile,.html,$(TEXTILE))

PYTHON ?= python

textile: $(TEXTILE)
html: $(HTML)

clean:
	rm -f $(HTML) $(TEXTILE)

.PRECIOUS: %.textile
%.html: %.textile
	@echo "Converting $< to $@"
	@$(PYTHON) -c "import sys,textile; print(textile.textile(sys.stdin.read()))" < $<  > $@-temp
	@mv $@-temp $@

%.textile: %.textile.in %.textile.vars Makefile
	@echo "Processing $< to $@"
	@cp $< $@
	@./vars.sh $@.vars $< $@
