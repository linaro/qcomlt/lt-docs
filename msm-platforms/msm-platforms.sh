#!/bin/sh

OUTPUT=$(mktemp)
OUTPUT_CSV=$(mktemp --suffix .csv)

echo "Year, SHA, Type, Subtype, Name" > ${OUTPUT_CSV}

for year in $(seq 2008 $(date +%Y)); do

    # Find 'last' commit of the year
    sha=$(git rev-list -1 --before="${year}-12-31" HEAD)
    echo "Year: $year - SHA: $sha"

    # very old stuff, platform files
    # mahimahi and sapphire have board file, but not include in Makefile, exclude them
    # trout and halibut use the same SoC, only count one for the SoC
    if git show ${sha}:arch/arm/mach-msm/ > ${OUTPUT} 2>&1 ; then
	grep 'board-.*\.c' ${OUTPUT} | \
	    grep -v '\(-panel\|-mmc\|-gpio\|-dt\|-mahimahi\|-sapphire\)' | \
	    while read -r line ; do
		echo "${year}, ${sha}, HW Device, Arm Board File, ${line}" >> ${OUTPUT_CSV}
	    done

	grep 'board-.*\.c' ${OUTPUT} | \
	    grep -v '\(-panel\|-mmc\|-gpio\|-dt\|-mahimahi\|-sapphire\|trout\)' | \
	    while read -r line ; do
		echo "${year}, ${sha}, SoC, Arm Board File, ${line}" >> ${OUTPUT_CSV}
	    done
    fi

    # arm32 DT
    if git show ${sha}:arch/arm/boot/dts/ > ${OUTPUT} 2>&1 ; then
	grep 'qcom-\(msm\|mdm\|ipq\|apq\|sdx\).*.dtsi' ${OUTPUT} | \
	    grep -v 'qcom-.*-' | \
	    while read -r line ; do
		echo "${year}, ${sha}, SoC, Arm32 DT Platform, ${line}" >> ${OUTPUT_CSV}
	    done

	grep 'qcom-\(msm\|mdm\|ipq\|apq\|sdx\).*.dts$' ${OUTPUT} | while read -r line ; do
	    echo "${year}, ${sha}, HW Device, Arm32 DT Board, ${line}" >> ${OUTPUT_CSV}
	done
    fi

    # arm64 DT
    if git show ${sha}:arch/arm64/boot/dts/qcom/ > ${OUTPUT} 2>&1 ; then
	grep '\(apq\|ipq\|msm\|sc\|sdm\|sm\|sa\|qcs\).*\.dtsi' ${OUTPUT} | \
	    grep -v '\-' | \
	    while read -r line ; do
		echo "${year}, ${sha}, SoC, Arm64 DT Plaform, ${line}" >> ${OUTPUT_CSV}
	    done

	grep '\(apq\|ipq\|msm\|sc\|sdm\|sm\|sa|\qcs\).*\.dts$' ${OUTPUT}  | while read -r line ; do
	    echo "${year}, ${sha}, HW Device, Arm64 DT Board, ${line}" >> ${OUTPUT_CSV}
	done

    fi

done

echo "Completed. Output file in ${OUTPUT_CSV}"
